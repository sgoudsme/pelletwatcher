import json
import requests
from bs4 import BeautifulSoup
from PelletItem import PelletItem


class Multibazar:
    pellet_list = list()
    URL = "https://www.multibazar.be/houtpellets-power-15kg-70-op-pallet-628967.html"

    def scrape_webpage(self):
        self.pellet_list = list()
        try:
            page = requests.get(self.URL)
            soup = BeautifulSoup(page.content, "html.parser")
            result = soup.find_all("div", class_="productPrice product-price")[0]

            price = result.find("span", class_="price").text.replace(',','.').replace('€', '').replace(' ','')

            self.pellet_list.append(PelletItem("Power+", float(price)))
        except:
            pass

    def create_json(self):
        return json.dumps([ob.__dict__ for ob in self.pellet_list])


if __name__ == '__main__':
    multi = Multibazar()
    multi.scrape_webpage()

    print(multi.create_json())






