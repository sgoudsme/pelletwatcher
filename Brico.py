import json
import requests
from bs4 import BeautifulSoup
from PelletItem import PelletItem


class Brico:
    pellet_list = list()
    URL = "https://www.brico.be/nl/badkamer-keuken-wonen/verwarming/brandstoffen/l346/?De+beste+prijzen=Vaste+lage+prijs&filterKeys=De+beste+prijzen"

    def scrape_webpage(self):
        self.pellet_list = list()
        try:
            page = requests.get(self.URL)
            soup = BeautifulSoup(page.content, "html.parser")
            results = soup.find_all("div", class_="mxd-grid mxd-side-panel-grid")[0]

            for item in results:
                if "ellet" in item.text:
                    name = item.find("meta", attrs={"data-product-name":True})['data-product-name']
                    price = item.find("div", attrs={"data-react-id":True}).text.replace(',','.')
                    try:
                        index_per_kg = price.index("per")
                        price = price[:index_per_kg]
                    except ValueError:
                        pass
                    self.pellet_list.append(PelletItem(name, float(price)))
        except:
            pass

    def create_json(self):
        return json.dumps([ob.__dict__ for ob in self.pellet_list])


if __name__ == '__main__':
    brico = Brico()
    brico.scrape_webpage()

    print(brico.create_json())






