import json
import requests
from bs4 import BeautifulSoup
from PelletItem import PelletItem


class BrandstoffenToon:
    pellet_list = list()
    URL = "https://www.pellets-brandhout.be/nl/briketten-en-andere/om-af-te-halen/pellets-per-zak"

    def scrape_webpage(self):
        self.pellet_list = list()
        try:
            page = requests.get(self.URL)
            soup = BeautifulSoup(page.content, "html.parser")
            results = soup.find(id="layer-product-list")
            job_elements = results.find_all("ol", class_="products list items product-items")[0]

            for pellet_container in job_elements:
                if len(pellet_container.text) > 1:
                    pellet_name = pellet_container.find("a", class_="product-item-link").text
                    pellet_price = pellet_container.find("span", class_="indekijker-item--price").text

                    self.pellet_list.append(PelletItem(self.cleanup_name(pellet_name), self.floatify_price(pellet_price)))
        except:
            pass

    def cleanup_name(self, str_name):
        return str_name.replace('*', '').replace(" (per zak) ", "")

    def floatify_price(self, str_price):
        str_price = str_price.replace(' ', '').replace('\xa0', '').replace(',', '.').replace('€', '')
        return float(str_price)

    def create_json(self):
        return json.dumps([ob.__dict__ for ob in self.pellet_list])


if __name__ == '__main__':
    toon = BrandstoffenToon()
    toon.scrape_webpage()

    print(toon.create_json())






