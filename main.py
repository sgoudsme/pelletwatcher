from MqttHandler import MqttHandler
from BrandstoffenToon import BrandstoffenToon
from Multibazar import Multibazar
from Brico import Brico
import asyncio
import yaml
import os

config_path = os.path.dirname(os.path.realpath(__file__))

with open(f"{config_path}/config.yaml") as file:
    config = yaml.load(file, Loader=yaml.FullLoader)

    HOST = config["host"]
    PUBLISH_TPC = config["publish_tpc"]
    SLEEP_TIME_M = config["update_time_m"]


async def main():
    brico = Brico()
    toon = BrandstoffenToon()
    multi = Multibazar()
    mqtt = MqttHandler(HOST, None, "empty", "empty", None)

    while True:
        brico.scrape_webpage()
        toon.scrape_webpage()
        multi.scrape_webpage()

        await asyncio.sleep(1)

        if len(toon.pellet_list) > 0:
            mqtt.publish(f"{PUBLISH_TPC}/BrandstoffenToon", toon.create_json())
        else:
            mqtt.publish(f"{PUBLISH_TPC}/BrandstoffenToon/Error", "List is empty")

        if len(brico.pellet_list) > 0:
            mqtt.publish(f"{PUBLISH_TPC}/Brico", brico.create_json())
        else:
            mqtt.publish(f"{PUBLISH_TPC}/Brico/Error", "List is empty")

        if len(multi.pellet_list) > 0:
            mqtt.publish(f"{PUBLISH_TPC}/Multibazar", multi.create_json())
        else:
            mqtt.publish(f"{PUBLISH_TPC}/Multibazar/Error", "List is empty")

        await asyncio.sleep(60*SLEEP_TIME_M)

if __name__ == '__main__':
    asyncio.run(main())















