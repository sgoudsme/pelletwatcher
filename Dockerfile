FROM python:3.10

RUN pip install paho-mqtt==1.5.0 pyyaml==5.1  requests==2.22.0 beautifulsoup4==4.11.1

ADD MqttHandler.py /home/
ADD BrandstoffenToon.py /home/
ADD Brico.py /home/
ADD Multibazar.py /home/
ADD PelletItem.py /home/

ADD main.py /home/

CMD [ "python", "-u", "/home/main.py" ]
